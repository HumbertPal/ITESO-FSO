# 1

Demostrar que los semáforos y los mensajes son equivalentes de la siguiente forma:

1. A partir de un sistema de paso de mensajes hay que implementar un semáforo entero, las primitivas que deberás definir son:

	1. `waitsem(buzón)`: Realizar la operación equivalente a wait para un semáforo
	2. `signalsem(buzón)`: Realizar la operación equivalente a signal para un semáforo
	3. `initsem(buzón,n)`: Realizar la operación equivalente a la inicialización del semáforo donde n será el valor inicial del semáforo.

```
waitsem(buzon)
{
	// bloqueante
	receive(buzon, null);
}

signalsem(buzon)
{
	// no bloqueante
	send(buzon, null);
}

initsem(buzon)
{
	send(buzon,null);
}
```

2. A partir de los semáforos, hay que implementar un sistema de paso de mensajes. 
Pista: haga uso de un área de elementos compartidos para contener un buzón, cada uno de ellos 
formado por un arreglo de espacios para los mensajes, y usar los semáforos para asegurar la 
exclusión mutua en el arreglo, asegurar que send no almacenará un nuevo mensaje en el arreglo 
y asegurar que receive no avanzará si el buzón está vacío.

```
struct buzon
{
	int elementos;
	char *msjs[MAX_MENSAJES];
	semaforo s;
	semaforo no_lleno, no_vacio;
}

send(struct &buzon, char *msj)
{
	if (elementos==MAX_MENSAJES) waitsem(buzon->no_lleno);

	waitsem(buzon->s);
	buzon->msjs[elementos] = malloc(strlen(msj)+1);
	strcpy(buzon->msjs[elementos], msj);
	elementos++;
	signalsem(buzon->s);
}

receive(struct buzon, char *msj)
{
	if (elementos==0) waitsem(buzon->no_vacio)

	waitsem(buzon->s);
	elementos--;
	strcpy(msj,buzon->msjs[elementos]);
	free(buzon->msjs[elementos]);
	signalsem(buzon->no_lleno);	
	signalsem(buzon->s);
}
```
