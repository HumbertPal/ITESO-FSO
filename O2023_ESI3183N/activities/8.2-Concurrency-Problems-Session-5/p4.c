    // Constantes

    const int K=2;
    const int N=4;

    // Variables
    int contador;
    int bloqueados=0;
    binarysem mutex=1;
    binarysem retardo=0;

    void proceso()
    {
 1:     wait(mutex);
 2:     if(contador==0)
        {
 3:         bloqueados++;
 4:         wait(retardo);
        }
        else
 5:         contador--;
 6:     signal(mutex);

        // Sección crítica

 7:     wait(mutex);
 8:     if(bloqueados>0)
        {
 9:         signal(retardo);
10:         bloqueados--;
        }
11:     else
12:         contador++;
13:     signal(mutex);
    }

    void main()
    {
14:     contador=K;
        cobegin
        {
15:         proceso(); proceso(); proceso();proceso();
        }
    }
