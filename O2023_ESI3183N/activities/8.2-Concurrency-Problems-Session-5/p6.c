/* 
El parque jurásico está formado por un museo de dinosaurios y un parque para excursionistas de safari. 
Hay m pasajeros y n coches monoplaza. Los pasajeros dan vueltas por el museo durante un tiempo y después
 se ponen en fila para dar un paseo en un coche de safari con el siguiente comporatamiento:

1. Cuando el coche está disponible, carga un pasajero, carga un pasajero y recorre el parque durante un tiempo aleatorio. 
2. Si los n coches están todos dando vueltas a los pasajeros, los que quieren subir deben esperar; 
3. Si un coche está listo para recoger pasajeros, pero no hay ninguno esperando, el coche debe esperar. 

Use semáforos para sincronizar los procesos de los m pasajeros con los m coches.
*/

Semaphore coche_vacio=0;
Semaphore tomar_coche=0;
Semaphore coche_lleno=0;
Semaphore pasaje_libre=0;

Pasajero()
{
    while(forever)
    {
        sleep(tiempo_aleatorio);
        wait(coche_vacio);
        signal(tomar_coche);
        wait(coche_lleno);
        wait(pasaje_libre);
    }
}

Coche()
{
    while(forever)
    {
        signal(coche_vacio);
        wait(tomar_coche);
        signal(coche_lleno);
        sleep(tiempo_aleatorio);
        signal(pasaje_libre);
    }
}

main()
{
    int i;
    cobegin
    {
         for(i=0;i<M;i++) Pasajero();
         for(i=0;i<N;i++) Coche();
    }
}
