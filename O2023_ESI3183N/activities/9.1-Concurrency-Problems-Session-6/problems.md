# 1

Del problema de los 4 jugadores de dominó sentados en una mesa jugando dominó, se van turnando para
tirar una ficha o pasar si no pueden jugar en el sentido contrario a las manecillas del reloj.
Ahora represente a los cuatro jugadores con procesos desde P(0) hasta P(3) y como sincronizaría 
los turnos usando monitores con variables de condición.

```
monitor jugador
{
	condition sea_turno;
	int turno;

	// inicializador
	init(int turno)
	{
		self.turno=turno
	}

	// el jugador espera turno
	turno(int i)
	{
		while (turno != i) cwait(sea_turno);
		// CS: el jugador juega y pasa al siguiente
		turno = (i+1) % 4;
        cnotify(turno);
	}
}

jugador j=new jugador(0);

Jugador(int i)
{
    while(true)
    {
		j.turno(i)
    }

}

main()
{
        turno=0;
        cobegin {
                Jugador(0);Jugador(1);Jugador(2);Jugador(3);
        }
}
```

# 2

En el caso del problema del productor consumidor que se muestra a continuación, la sincronización se hace con un monitor
con notificación definido a partir de semáforos. Para esto fue necesario implantar las funciones:

- `entrar_monitor()`: Permite que solo un proceso/hilo entre a esa sección ya considerada como parte del monitor.
- `cwait()`: Bloquea un proceso/hilo dentro del monitor.
- `cnotify()`: Desbloquea un proceso/hilo que esté bloqueado en la zona de espera del monitor.
- `leave_monitor()`: Permite que otro proceso/hilo entre al monitor.

El problema es que la librería donde se definen esta funciones se extravió, por lo que es necesario re-definirlas.

Utilizando pseudocódigo defina usando semáforos las funciones : `entrar_monitor()`, `cwait()`, `cnotify()` y `leave_monitor()`
para que, `agrega_al_buffer()` y `tomar_del_buffer()` se comporten como procedimientos de un monitor.

```
#define MAX_CAPACIDAD 10
int elementos;
Semaforo sem_monitor, sem_proceso;

int entrar_monitor()
{
	semwait(sem_monitor);
}

int leave_monitor()
{
	semsignal(sem_monitor);
}

int cwait()
{
	semwait(sem_proceso);
}

int cnotify()
{
	semsignal(sem_proceso);
}

int agrega_al_buffer(e)
{
    while(elementos==MAX_CAPACIDAD)
        cwait();

    enter_monitor();

    ent++;
    end=ent%MAX_CAPACIDAD;
    buffer[ent]=e;
    elementos++;

    leave_monitor();
    cnotify();

}

int tomar_del_buffer()
{
    while(elementos==0)
        cwait();

    enter_monitor();

    sal++;
    sal=sal%MAX_CAPACIDAD;
    e=buffer[sal];
    elementos-- ;

    leave_monitor();
    cnotify();

    return(e);
}

int Productor()
{
    int e;
    while(forever)
    {
        e=produce();
        agrega_al_buffer(e);
    }
}

int Consumidor()
{
    int e;
    while(forever)
    {
        e=tomar_del_buffer();
        consumir(e);
    }
}

int main()
{
	elementos=0;
	sem_monitor=1;
	sem_proceso=0;
	cobegin {
		Productor();Consumidor();
	}
```
