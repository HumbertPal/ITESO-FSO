# 1

Del problema de los 4 jugadores de dominó sentados en una mesa jugando dominó,
se van turnando para tirar una ficha o pasar si no pueden jugar en el sentido
contrario a las manecillas del reloj. Ahora represente a los cuatro jugadores
con procesos desde P(0) hasta P(3) y como sincronizaría los turnos usando un sistema de paso de mensajes.


```
#define JUGADORES 4
Jugador(i)
{
	string str;
	int next;

    while(true)
    {
        next = (i+1) % JUGADORES;
		receive(buzones[i],str,BLOCKING)
        // CS: paso o juego
		str='turn'
        send(buzones[next],str,NON_BLOCKING);
    }
}

main()
{
        buzon buzones[JUGADORES]= {'turn',null,null,null}
        cobegin {
			Jugador(0);...;Jugador(JUGADORES-1);
        }
}
```
