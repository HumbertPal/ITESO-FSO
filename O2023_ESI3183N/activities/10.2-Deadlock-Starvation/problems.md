# 1
De un ejemplo de bloqueo mutuo en el que participe solamente un proceso, ¿Es posible eso?, explique su respuesta

No, por definicion no se cumple la condicion 'Espera Circular'

Ver slides sobre las condiciones del interbloqueo

# 2

Cite tres ejemplos de bloqueos mutuos no relacionados con un entorno de sistema de computación.

- Entrada a un elevador o el metro, donde los que de adentro estan esperando salir pero son bloqueados por los
de afuera y viceversa

- Dos personas caminando en un espacio muy reducido, donde alguno de estos se tiene que regresar para que el otro pase.
si ninguno de los dos cede, las dos personas se quedaran atrapadas

- en un pista de aterrizaje hay dos aviones, uno por aterrizar y otro por despegar. la torre de mando se queda incomunicada
y los pilotos no pueden realizar sus tareas
 

# 3
Proporcione un ejemplo sencillo de bloqueo mutuo de recursos en el que participen tres procesos y tres recursos. Dibuje la gráfica apropiada de asignación de recursos.

# 4
Considere el ejemplo de bloqueo mutuo de tráfico que se muestra en los vídeos previos de la sesión

Demuestre que las cuatro condiciones necesarias para el bloqueo mutuo se cumplen en este ejemplo.
plantee una regla sencilla que evite los bloqueos mutuos en tal sistema

- Exclusion mutua
  Los 4 vehiculos involucrados se encuentran en su primer seccion critica, y en este momento
  se tiene la exclusion mutua de dichas secciones pero haria falta tomar otra

- Retencion y Espera
  Una vez obtenida la primera (de dos) seccion critica, esperan que se libere la siguiente seccion pero esto no
  ocurre, ya que los demas vehiculos estan esperando lo mismo

- No expropiacion
  Asumiendo que los coches no se pueden echar en reversa o que hay vehiculos detras de ellos, la seccion critica 
  que cada vehiculo obtuvo no puede ser liberada (expropiada)

- Espera Circular
  V1 espera a V2
  V2 espera a V3
  V3 espera a V4
  V4 espera a V1

