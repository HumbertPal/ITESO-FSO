## Goal

Understand the relation between indexing (dereferences) and page faults

## Class Activity

This is an individual activity to be reviewed each at the end of the class.

Questions:

- What is a page fault?
- How is an matrix stored in the memory? row-order or column order?
- How defines the order? language ? compiler? OS?

1. Create a repository at gitlab, where it will contain the following files
   1. Makefile
   2. matrix_mult.c
   3. README.md
   4. optional: matrix_mult.f
   
`matrix_mult.c` is a program that generates populates a matrix randomly and
accepts a single parameter that defines the how population would be done, *row-order*
or *column-order*. The `README.md` is a markdown text file that answers the questions
above. The `Makefile` would contain three rules: `build`, `profile` and `clean`;
the `profile` rule would use any performance tool, e.g `perf`, to count the number of page faults
for the row-order and column-order versions of the program.
