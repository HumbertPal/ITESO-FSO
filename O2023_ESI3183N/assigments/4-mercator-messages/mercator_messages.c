#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

#define BLOCKING 0
#define NON_BLOCKING IPC_NOWAIT


struct msgbuf {
  long mtype;    /* message type, must be > 0 */
  double mtext;    /* partial sum */
};

double x = 1.0;

int queue_id;    // Buzón de mensajes

double get_member(int n, double x)
{
  int i;
  double numerator = 1;
  for(i=0; i<n; i++ )
    numerator = numerator*x;
  if (n % 2 == 0)
    return ( - numerator / n );
  else
    return numerator/n;
}

void proc(int proc_num)
{
  int i;
  struct msgbuf msg;
  double sum=0;

  // wait to start
  msgrcv(queue_id, (void *) &msg, sizeof(msg.mtext), proc_num, BLOCKING);

  for(i=proc_num-1; i<SERIES_MEMBER_COUNT;i+=NPROCS)
    sum += get_member(i+1, x);

  msg.mtext = sum;

  printf("child %d %f\n", proc_num, msg.mtext);
  // send a message for completion
  //msg.mtype = proc_num;
  msgsnd(queue_id, (void *) &msg, sizeof(msg.mtext), NON_BLOCKING);

  exit(0);
}

void master_proc()
{
  int i;
  struct msgbuf msg;
  double sums=0;

  // start all child processes
  for(i=0; i<NPROCS; i++) {
    msg.mtype = i+1;
    if (msgsnd(queue_id, (void *) &msg, sizeof(msg.mtext),NON_BLOCKING) == -1) {
      perror("msgsnd error");
      exit(EXIT_FAILURE);
    }
  }

  // wait for completion
  for(i=0; i<NPROCS; i++) {
    if (msgrcv(queue_id, (void *) &msg, sizeof(msg.mtext), i+1, BLOCKING) == -1) {
      if (errno != ENOMSG) {
	perror("msgrcv");
	exit(EXIT_FAILURE);
      }
    }
    sums += msg.mtext;
  }

  // send the result to main process
  msg.mtype = NPROCS+1;
  msg.mtext = sums;
  if (msgsnd(queue_id, (void *) &msg, sizeof(msg.mtext), NON_BLOCKING) == -1) {
    perror("msgsnd error");
    exit(EXIT_FAILURE);
  }

  exit(0);
}

int main()
{
  long long start_ts;
  long long stop_ts;
  long long elapsed_time;
  struct timeval ts;
  struct msgbuf msg;
  int i;
  double sums;

  queue_id = msgget(0x5678,0666|IPC_CREAT);

  if(queue_id == -1) {
    fprintf(stderr,"No se pudo crear el buzón\n");
    exit(1);
  }

  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec; // Tiempo inicial

  for(i=0; i<NPROCS;i++)
    if (!fork())
      proc(i+1);

  if(!fork())
    master_proc();

  for(i=0;i<NPROCS+1;i++) {
    wait(NULL);
  }

  if (msgrcv(queue_id, (void *) &msg, sizeof(msg.mtext), NPROCS+1, BLOCKING) == -1) {
    if (errno != ENOMSG) {
      perror("msgrcv");
      exit(EXIT_FAILURE);
    }
  }

  sums = msg.mtext;

  msgctl(queue_id, IPC_RMID, NULL);

  gettimeofday(&ts, NULL);
  stop_ts = ts.tv_sec; // Tiempo final
  elapsed_time = stop_ts - start_ts;

  printf("Tiempo = %lld segundos\n", elapsed_time);
  printf("El resultado es %10.8f\n", sums);
}
