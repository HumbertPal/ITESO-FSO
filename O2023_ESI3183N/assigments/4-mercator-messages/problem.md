# Mercator - Messages


El programa que se muestra a continuación realiza el cálculo de `ln(1+x)` donde `x = 1` por medio
de la serie de Mercator. Para hacer este cálculo de manera precisa, la sumatoria se realiza con
`200,000` términos, lo cual implica mucho trabajo de procesamiento. Para que en este trabajo se
aproveche la capacidad que brinda un procesador Multicore, éste se reparte en 4 procesos que
pueden ejecutarse en paralelo.

El programa que se muestra en la Figura 1 crea un proceso que llamaremos proceso maestro, y
cuatro procesos que serán los procesos esclavos quienes realizan los cálculos. El programa
principal crea 5 procesos que son el maestro y 4 esclavos. El proceso maestro establece el valor
de una bandera que les indica a los procesos esclavos que pueden iniciar los cálculos, y mientras
los esclavos hacen el trabajo el proceso maestro esperará a que estos terminen. Todos los
procesos se comunican y sincronizan a través de variables en memoria compartida.

Aunque esta solución funciona, esta no es eficiente ya que tanto el maestro como esclavos tienen
espera ocupada. Además, es probable que en algunas ejecuciones exista un error cuando se
realiza el conteo de procesos.

Ahora hay que utilizar un sistema de paso de mensajes para arreglar el código de manera que
genere espera ocupada. La intención es lograr tanto la sincronización como la comunicación con
mensajes por lo que es posible prescindir de la utilización de memoria compartida.
