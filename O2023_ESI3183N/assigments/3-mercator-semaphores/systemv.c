#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/time.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>

#define NPROCS 4
#define SERIES_MEMBER_COUNT 200000

double *sums;
double x = 1.0;

double *res;

int semid;

double get_member(int n, double x)
{
  int i;
  double numerator = 1;
  for(i=0; i<n; i++ )
    numerator = numerator*x;
  if (n % 2 == 0)
    return ( - numerator / n );
  else
    return numerator/n;
}

void proc(int proc_num)
{
  int i;

  struct sembuf wait_for_master = {0,-1,0};
  struct sembuf signal_master = {0,1,0};

  // wait until master_proc decides when to start
  semop(semid,&wait_for_master,1);

  sums[proc_num] = 0;
  for(i=proc_num; i<SERIES_MEMBER_COUNT;i+=NPROCS)
    sums[proc_num] += get_member(i+1, x);
  
  semop(semid,&signal_master,1);

  exit(0);
}

void master_proc()
{
  int i;
  struct sembuf start_all = {0,4,0};
  struct sembuf wait_all  = {0,-4,0};

  // start all processes
  semop(semid,&start_all,1);

  // wait for completion
  semop(semid,&wait_all,1);

  *res = 0;
  for(i=0; i<NPROCS; i++)*res += sums[i];
  exit(0);
}

int main()
{
  long long start_ts;
  long long stop_ts;
  long long elapsed_time;
  struct timeval ts;
  int i;
  int p;
  int shmid;
  void *shmstart;

  // create a single semaphore
  semid=semget(0x4008,1,0666|IPC_CREAT);
  semctl(semid,0,SETVAL,0);

  shmid = shmget(0x1234,(NPROCS+1)*sizeof(double),0666|IPC_CREAT);
  shmstart = shmat(shmid,NULL,0);
  sums = shmstart;

  res = shmstart + NPROCS * sizeof(double);

  gettimeofday(&ts, NULL);
  start_ts = ts.tv_sec; // Tiempo inicial
  for(i=0; i<NPROCS;i++)
    {
      p = fork();
      if(p==0) proc(i);
    }
  p = fork();
  if(p==0) master_proc();

  printf("El recuento de ln(1 + x) miembros de la serie de Mercator es %d\n",SERIES_MEMBER_COUNT);
  printf("El valor del argumento x es %f\n", (double)x);

  for(int i=0;i<NPROCS+1;i++)
    wait(NULL);

  gettimeofday(&ts, NULL);
  stop_ts = ts.tv_sec; // Tiempo final
  elapsed_time = stop_ts - start_ts;

  printf("Tiempo = %lld segundos\n", elapsed_time);
  printf("El resultado es %10.8f\n", *res);
  printf("Llamando a la función ln(1 + %f) = %10.8f\n",x, log1p(x));

  shmdt(shmstart);
  shmctl(shmid,IPC_RMID,NULL);
}
